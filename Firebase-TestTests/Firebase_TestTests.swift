//
//  Firebase_TestTests.swift
//  Firebase-TestTests
//
//  Created by Vadim Krutov on 25/10/2017.
//  Copyright © 2017 Vadim Krutov. All rights reserved.
//

import XCTest
import Firebase
import PromiseKit
import AwaitKit

class Firebase_TestTests: XCTestCase {
    
    var ref: DatabaseReference!
    
    override func setUp() {
        super.setUp()
        
        self.ref = Database.database().reference()
    }
    
    func testExample() {
        let data: [String: Any] = [
            "test": 1,
            "test2": "user"
        ]
        
        let promise: Promise<Bool> = Promise { resolve, reject in
            ref.child("test/test/test").setValue(data) { err, _ in
                if err == nil {
                    resolve(true)
                } else {
                    reject(err!)
                }
            }
        }
        
        let isStored = try! await(promise)
        
        XCTAssert(isStored)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
